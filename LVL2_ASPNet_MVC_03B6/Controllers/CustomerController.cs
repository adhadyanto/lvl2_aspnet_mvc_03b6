﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LVL2_ASPNet_MVC_03B6.Models;

namespace LVL2_ASPNet_MVC_03B6.Controllers
{

    public class CustomerController : Controller
    {
        db_customer_dicky db_Model = new db_customer_dicky();



        // GET: Customer
        public ActionResult Index()
        {
            return View(db_Model.tbl_customer_dicky.ToList());
        }



        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(db_Model.tbl_customer_dicky.Where(x => x.Id == id).FirstOrDefault());
        }



        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }



        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(tbl_customer_dicky customer)
        {
            using (var transactions = db_Model.Database.BeginTransaction())
            {
                try
                {
                    // TODO: Add insert logic here
                    db_Model.tbl_customer_dicky.Add(customer);
                    db_Model.SaveChanges();



                    tbl_customer_dicky Edit = db_Model.tbl_customer_dicky.Where(b => b.Id == 1).FirstOrDefault();
                    Edit.Description = "Jakarta Barat";
                    db_Model.Entry(Edit).State = EntityState.Modified;
                    db_Model.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Index");
                }
                /*
                catch(DbEntityValidationException err)
                {
                    ViewBag.err = "Invalid length";
                    return View();
                }*/
                catch (Exception err)
                {
                    var ErrorPos = "Create_Customer";
                    db_customer_dicky dbError = new db_customer_dicky();
                    tbl_log_history error = new tbl_log_history
                    {
                        ErrorMessage = err.Message,
                        ErrorPosition = ErrorPos,
                        ErrorDate = DateTime.Now
                    };
                    dbError.tbl_log_history.Add(error);
                    dbError.SaveChanges();
                    transactions.Rollback();
                    ViewBag.err = err.Message;
                    return View();
                }
            }
        }



        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db_Model.tbl_customer_dicky.Where(x => x.Id == id).FirstOrDefault());
        }



        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, tbl_customer_dicky customer)
        {
            try
            {
                // TODO: Add update logic here
                db_Model.Entry(customer).State = EntityState.Modified;
                db_Model.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }



        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(db_Model.tbl_customer_dicky.Where(x => x.Id == id).FirstOrDefault());
        }



        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, tbl_customer_dicky customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer = db_Model.tbl_customer_dicky.Where(x => x.Id == id).FirstOrDefault();
                db_Model.tbl_customer_dicky.Remove(customer);
                db_Model.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
