﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LVL2_ASPNet_MVC_03B6.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class db_customer_dicky : DbContext
    {
        public db_customer_dicky()
            : base("name=db_customer_dicky")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tbl_customer_dicky> tbl_customer_dicky { get; set; }
        public virtual DbSet<tbl_log_history> tbl_log_history { get; set; }
    }
}
